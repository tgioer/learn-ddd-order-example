package com.mallfoundry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnDddOrderExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnDddOrderExampleApplication.class, args);
    }

}
